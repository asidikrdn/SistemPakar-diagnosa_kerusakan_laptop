import { useState } from "react";
import { Container, Table, Row, Col, Form, Button } from "react-bootstrap";

const tabelGejala = [
  { id: "G01", gejala: "Laptop sering hang", bobotPakar: 0.6 },
  {
    id: "G02",
    gejala: "Temperatur kipas tidak dalam kondisi baik",
    bobotPakar: 0.6,
  },
  { id: "G03", gejala: "Laptop cepat panas", bobotPakar: 0.4 },
  { id: "G04", gejala: "Laptop sering overheat", bobotPakar: 0.8 },
  { id: "G05", gejala: "Kipas berisik", bobotPakar: 0.8 },
  { id: "G06", gejala: "Terdapat angin di lubang sirkulasi", bobotPakar: 0.6 },
  { id: "G07", gejala: "Laptop sering mati tiba-tiba", bobotPakar: 0.4 },
  { id: "G08", gejala: "Kipas tidak bersirkulasi", bobotPakar: 0.4 },
  { id: "G09", gejala: "Keyboard tidak dapat digunakan", bobotPakar: 0.8 },
  { id: "G10", gejala: "Terdapat kode aneh", bobotPakar: 0.8 },
  { id: "G11", gejala: "Tampil kode error pada laptop", bobotPakar: 0.8 },
  { id: "G12", gejala: "Hardisk overheat", bobotPakar: 0.6 },
  { id: "G13", gejala: "Hardisk mudah panas", bobotPakar: 0.6 },
  { id: "G14", gejala: "Data seringkali tidak terbaca", bobotPakar: 0.6 },
  { id: "G15", gejala: "Bluescreen", bobotPakar: 0.6 },
  { id: "G16", gejala: "Laptop tidak dapat bekerja", bobotPakar: 0.6 },
  { id: "G17", gejala: "---", bobotPakar: 0.0 },
  { id: "G18", gejala: "Gagal membuka aplikasi", bobotPakar: 0.6 },
  {
    id: "G19",
    gejala: "Sistem hanya aktif sementara (sebentar)",
    bobotPakar: 0.6,
  },
  { id: "G20", gejala: "Sering gagal booting", bobotPakar: 0.8 },
  { id: "G21", gejala: "Layar bergetar", bobotPakar: 0.4 },
  { id: "G22", gejala: "Terdapat bintik putih pada layar", bobotPakar: 0.6 },
  { id: "G23", gejala: "Layar bergaris", bobotPakar: 0.8 },
  { id: "G24", gejala: "Terdapat artifact pada layar", bobotPakar: 0.8 },
  { id: "G25", gejala: "Layar blank", bobotPakar: 0.4 },
  {
    id: "G26",
    gejala: "Booting terhenti setelah proses POST",
    bobotPakar: 0.4,
  },
  {
    id: "G27",
    gejala: "Booting ke windows berjalan sangat lambat",
    bobotPakar: 0.6,
  },
  { id: "G28", gejala: "Windows explorer tidak dapat dibuka", bobotPakar: 0.6 },
  { id: "G29", gejala: "Start Menu tidak bekerja", bobotPakar: 0.8 },
  {
    id: "G30",
    gejala: "Proses Shutdown tidak berjalan dengan baik",
    bobotPakar: 0.8,
  },
  {
    id: "G31",
    gejala: "Proses Shutdown berhenti sebelum komputer benar-benar mati",
    bobotPakar: 0.6,
  },
  { id: "G32", gejala: "Layar selalu senyap", bobotPakar: 0.6 },
  { id: "G33", gejala: "Driver ter-uninstall", bobotPakar: 0.4 },
  { id: "G34", gejala: "USB port longgar", bobotPakar: 0.8 },
  { id: "G35", gejala: "USB port rusak", bobotPakar: 0.6 },
  { id: "G36", gejala: "Jalur USB di PCB terputus", bobotPakar: 0.6 },
  { id: "G37", gejala: "Tidak bisa menyeleksi USB", bobotPakar: 0.8 },
  { id: "G38", gejala: "Proses charging bermasalah", bobotPakar: 0.8 },
  { id: "G39", gejala: "Charger laptop bermasalah", bobotPakar: 0.6 },
  { id: "G40", gejala: "Adaptop charger bermasalah", bobotPakar: 0.6 },
  { id: "G41", gejala: "Kabel charger bermasalah", bobotPakar: 0.8 },
  { id: "G42", gejala: "Konektor charger bermasalah", bobotPakar: 0.6 },
  { id: "G43", gejala: "Port charger pada laptop bermasalah", bobotPakar: 0.8 },
  {
    id: "G44",
    gejala: "Muncul tanda silang pada logo baterai",
    bobotPakar: 0.8,
  },
];

const tabelBobot = [
  { id: "B01", bobot: 0, keterangan: "Tidak Yakin" },
  { id: "B02", bobot: 0.4, keterangan: "Sedikit Yakin" },
  { id: "B03", bobot: 0.6, keterangan: "Cukup Yakin" },
  { id: "B04", bobot: 0.8, keterangan: "Yakin" },
];

const tabelKerusakan = [
  {
    id: "K01",
    keterangan: "Kerusakan terjadi pada Kipas",
    kalkulasiCF: [
      { idGejala: "G01", bobotKombinasi: 0 },
      { idGejala: "G02", bobotKombinasi: 0 },
      { idGejala: "G03", bobotKombinasi: 0 },
      { idGejala: "G04", bobotKombinasi: 0 },
      { idGejala: "G05", bobotKombinasi: 0 },
      { idGejala: "G06", bobotKombinasi: 0 },
      { idGejala: "G07", bobotKombinasi: 0 },
      { idGejala: "G08", bobotKombinasi: 0 },
      { idGejala: "G09", bobotKombinasi: 0 },
    ],
    score: 0,
  },
  {
    id: "K02",
    keterangan: "Kerusakan terjadi pada Hardisk",
    kalkulasiCF: [
      { idGejala: "G10", bobotKombinasi: 0 },
      { idGejala: "G11", bobotKombinasi: 0 },
      { idGejala: "G12", bobotKombinasi: 0 },
      { idGejala: "G13", bobotKombinasi: 0 },
      { idGejala: "G14", bobotKombinasi: 0 },
      { idGejala: "G15", bobotKombinasi: 0 },
    ],
    score: 0,
  },
  {
    id: "K03",
    keterangan: "Kerusakan terjadi pada Processor",
    kalkulasiCF: [
      { idGejala: "G01", bobotKombinasi: 0 },
      { idGejala: "G03", bobotKombinasi: 0 },
      { idGejala: "G07", bobotKombinasi: 0 },
      { idGejala: "G15", bobotKombinasi: 0 },
      { idGejala: "G16", bobotKombinasi: 0 },
      { idGejala: "G18", bobotKombinasi: 0 },
      { idGejala: "G19", bobotKombinasi: 0 },
      { idGejala: "G20", bobotKombinasi: 0 },
    ],
    score: 0,
  },
  {
    id: "K04",
    keterangan: "Kerusakan terjadi pada Layar",
    kalkulasiCF: [
      { idGejala: "G21", bobotKombinasi: 0 },
      { idGejala: "G22", bobotKombinasi: 0 },
      { idGejala: "G23", bobotKombinasi: 0 },
      { idGejala: "G24", bobotKombinasi: 0 },
      { idGejala: "G25", bobotKombinasi: 0 },
    ],
    score: 0,
  },
  {
    id: "K05",
    keterangan: "Kerusakan terjadi pada Sistem Operasi",
    kalkulasiCF: [
      { idGejala: "G26", bobotKombinasi: 0 },
      { idGejala: "G27", bobotKombinasi: 0 },
      { idGejala: "G28", bobotKombinasi: 0 },
      { idGejala: "G29", bobotKombinasi: 0 },
      { idGejala: "G30", bobotKombinasi: 0 },
      { idGejala: "G31", bobotKombinasi: 0 },
      { idGejala: "G32", bobotKombinasi: 0 },
    ],
    score: 0,
  },
  {
    id: "K06",
    keterangan: "Kerusakan terjadi pada USB",
    kalkulasiCF: [
      { idGejala: "G33", bobotKombinasi: 0 },
      { idGejala: "G34", bobotKombinasi: 0 },
      { idGejala: "G35", bobotKombinasi: 0 },
      { idGejala: "G36", bobotKombinasi: 0 },
      { idGejala: "G37", bobotKombinasi: 0 },
    ],
    score: 0,
  },
  {
    id: "K07",
    keterangan: "Kerusakan terjadi pada Sistem Pengecasan",
    kalkulasiCF: [
      { idGejala: "G03", bobotKombinasi: 0 },
      { idGejala: "G38", bobotKombinasi: 0 },
      { idGejala: "G39", bobotKombinasi: 0 },
      { idGejala: "G40", bobotKombinasi: 0 },
      { idGejala: "G41", bobotKombinasi: 0 },
      { idGejala: "G42", bobotKombinasi: 0 },
      { idGejala: "G43", bobotKombinasi: 0 },
      { idGejala: "G44", bobotKombinasi: 0 },
    ],
    score: 0,
  },
];

const App = () => {
  const [input, setInput] = useState({
    kodeGejala: "",
    bobotUser: 0,
  });
  const [tabelInputGejala, setTabelInputGejala] = useState([]);
  const [dataKerusakan, setDataKerusakan] = useState(tabelKerusakan);

  const handleInputChange = (e) => {
    e.target.name === "bobotUser"
      ? setInput((prevState) => ({
          ...prevState,
          [e.target.name]: parseFloat(e.target.value),
        }))
      : setInput((prevState) => ({
          ...prevState,
          [e.target.name]: e.target.value,
        }));
  };

  const handleTambahGejala = (e) => {
    e.preventDefault();

    if (tabelInputGejala.some((el) => el.kodeGejala === input.kodeGejala)) {
      let indexDataEdited = tabelInputGejala.findIndex(
        (el) => el.kodeGejala === input.kodeGejala
      );

      let newTabelInputGejala = [...tabelInputGejala];

      newTabelInputGejala.splice(indexDataEdited, 1, input);

      setTabelInputGejala(newTabelInputGejala);
    } else {
      setTabelInputGejala((prevState) => [...prevState, input]);
    }

    setInput((prevState) => ({ ...prevState, kodeGejala: "" }));
  };

  const handleDiagnosaKerusakan = () => {
    const hitungScoreKerusakan = (idKerusakan) => {
      let kerusakan;

      kerusakan = dataKerusakan.find((el) => el.id === idKerusakan);

      kerusakan.kalkulasiCF = kerusakan.kalkulasiCF.map((el) => {
        let id = el.idGejala;

        let cfPakar;
        tabelGejala.forEach((data) => {
          if (data.id === id) {
            cfPakar = data.bobotPakar;
          }
        });

        let cfUser;
        tabelInputGejala.forEach((data) => {
          if (data.kodeGejala === id) {
            cfUser = data.bobotUser;
            // console.log(
            //   `data gejala ${data.kodeGejala} adalah ${data.bobotUser}`
            // );
            // console.log(cfUser);
          }
        });

        // CFcombine = CFpakar * CFuser
        let bobotKombinasi = cfPakar * (cfUser !== undefined ? cfUser : 0);
        // console.log(id, cfPakar, cfUser, bobotKombinasi);

        return { ...el, bobotKombinasi: bobotKombinasi };
      });

      kerusakan.score = kerusakan.kalkulasiCF.reduce(
        (total, el) => total + el.bobotKombinasi * (1 - total),
        0
      );

      // return kerusakan;
      let indexKerusakan = dataKerusakan.findIndex(
        (el) => el.id === idKerusakan
      );
      let newDataKerusakan = dataKerusakan;
      newDataKerusakan.splice(indexKerusakan, 1, kerusakan);
      setDataKerusakan(newDataKerusakan);
    };

    // Kerusakan kipas
    hitungScoreKerusakan("K01");
    // let K01 = hitungScoreKerusakan("K01");

    // Kerusakan Hardisk
    hitungScoreKerusakan("K02");
    // let K02 = hitungScoreKerusakan("K02");

    // Kerusakan Processor
    hitungScoreKerusakan("K03");
    // let K03 = hitungScoreKerusakan("K03");

    // Kerusakan Sistem Layar
    hitungScoreKerusakan("K04");
    // let K04 = hitungScoreKerusakan("K04");

    // Kerusakan Sistem Operasi
    hitungScoreKerusakan("K05");
    // let K05 = hitungScoreKerusakan("K05");

    // Kerusakan USB
    hitungScoreKerusakan("K06");
    // let K06 = hitungScoreKerusakan("K06");

    // Kerusakan Sistem Pengecasan
    hitungScoreKerusakan("K07");
    // let K07 = hitungScoreKerusakan("K07");

    setDataKerusakan((prev) => [...prev]);
    // setDataKerusakan([K01, K02, K03, K04, K05, K06, K07]);
  };

  // console.log(input);
  // console.log(tabelInputGejala);
  // console.log(dataKerusakan);

  return (
    <>
      <h1 className="text-center">Diagnosa Kerusakan Laptop</h1>

      <Container className="mt-5">
        <Row>
          <Col>
            <h3 className="text-center">Form Tambah Gejala</h3>
            <hr className="w-75 mx-auto" />
          </Col>
        </Row>
        <Form onSubmit={handleTambahGejala}>
          <Row>
            <Col md={5}>
              <Form.Group className="mb-3" controlId="formGejala">
                <Form.Label>Gejala :</Form.Label>
                <Form.Select
                  aria-label="Gejala"
                  name="kodeGejala"
                  onChange={handleInputChange}
                >
                  <option
                    value=""
                    selected={input.kodeGejala === "" ? true : false}
                  >
                    Pilih Gejala
                  </option>
                  ;
                  {tabelGejala.map((el) => {
                    return (
                      <option value={el.id} key={el.id}>
                        {el.gejala}
                      </option>
                    );
                  })}
                </Form.Select>
              </Form.Group>
            </Col>
            <Col md={5}>
              <Form.Group className="mb-3" controlId="formBobot">
                <Form.Label>Bobot :</Form.Label>
                <Form.Select
                  aria-label="Bobot"
                  name="bobotUser"
                  onChange={handleInputChange}
                  disabled={input.kodeGejala === "" ? true : false}
                >
                  {tabelBobot.map((el, i) => {
                    return (
                      <option value={el.bobot} key={i}>
                        {el.keterangan}
                      </option>
                    );
                  })}
                </Form.Select>
              </Form.Group>
            </Col>
            <Col md={2}>
              <p className="d-md-block d-none mb-1 opacity-0">Tombol Tambah</p>
              <Button variant="primary" type="submit" className="w-100">
                Tambah Gejala
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
      <Container fluid>
        <Row className="mt-5">
          <Col xs={12}>
            <h3 className="text-center">Tabel Gejala</h3>
            <hr className="w-75 mx-auto" />
          </Col>
          <Col xs={12} className="text-center">
            <Table striped bordered hover className="w-75 mx-auto">
              <thead>
                <tr className="text-center" style={{ verticalAlign: "middle" }}>
                  <th className="w-75">Gejala Kerusakan</th>
                  <th className="w-25">Bobot</th>
                </tr>
              </thead>
              <tbody>
                {tabelInputGejala.map((data) => {
                  return (
                    <tr className="text-center" key={data.kodeGejala}>
                      <td>
                        {tabelGejala.map((el) => {
                          return el.id === data.kodeGejala && el.gejala;
                        })}
                      </td>
                      <td>
                        {tabelBobot.map((el) => {
                          return el.bobot === data.bobotUser && el.keterangan;
                        })}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
            <Button variant="success" onClick={handleDiagnosaKerusakan}>
              Cek Kerusakan
            </Button>
          </Col>
        </Row>
      </Container>
      <Container fluid>
        <Row className="mt-5">
          <Col xs={12}>
            <h3 className="text-center">Hasil Diagnosa</h3>
            <hr className="w-75 mx-auto" />
          </Col>
          <Col xs={12} className="text-center">
            <Table striped bordered hover className="w-75 mx-auto">
              <thead>
                <tr className="text-center" style={{ verticalAlign: "middle" }}>
                  <th className="w-75">Jenis Kerusakan</th>
                  <th className="w-25">Nilai Kepastian (CF)</th>
                </tr>
              </thead>
              <tbody>
                {dataKerusakan.map((data, i) => {
                  return (
                    data.score !== 0 && (
                      <tr className="text-center" key={i}>
                        <td>{data.keterangan}</td>
                        <td>{Math.round(data.score * 100)} %</td>
                      </tr>
                    )
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default App;
